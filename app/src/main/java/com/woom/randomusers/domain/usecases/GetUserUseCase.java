package com.woom.randomusers.domain.usecases;

import com.squareup.otto.Bus;
import com.woom.randomusers.domain.BaseUseCase;
import com.woom.randomusers.domain.BaseUseCase;
import com.woom.randomusers.domain.busresponses.UserDetailResponse;
import com.woom.randomusers.domain.busresponses.UsersResponse;
import com.woom.randomusers.repository.UserRepository;

import java.util.List;

public class GetUserUseCase extends BaseUseCase<UserDetailResponse> {

    private UserRepository repository;

    public GetUserUseCase(Bus bus, UserRepository repository) {
        super(bus);
        this.repository = repository;
    }

    @Override
    public void execute() {
        List<String> params = getParams();
        String userId = params.get(0);
        handleResponse(repository.getByKey(userId));
    }
}
