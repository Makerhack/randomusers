package com.woom.randomusers.domain.busresponses;

import com.woom.randomusers.domain.model.User;
import com.woom.randomusers.domain.model.User;

public class UserDetailResponse extends DataResponse<User> {

    public UserDetailResponse(){
        super();
    }

    public UserDetailResponse(User user){
        super(user);
    }

    public UserDetailResponse(DataResponse<User> userDataResponse){
        super(userDataResponse);
    }
}
