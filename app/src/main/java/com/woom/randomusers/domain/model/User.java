package com.woom.randomusers.domain.model;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;

import java.io.Serializable;

public class User implements Comparable<User>, Serializable {

    @DatabaseField(generatedId = true)
    protected int _id;

    @SerializedName("gender")
    @DatabaseField
    protected String gender;

    @SerializedName("name")
    @DatabaseField(dataType = DataType.SERIALIZABLE)
    protected Name name;

    @SerializedName("location")
    @DatabaseField(dataType = DataType.SERIALIZABLE)
    protected Location location;

    @SerializedName("email")
    @DatabaseField
    protected String email;

    @DatabaseField(dataType = DataType.SERIALIZABLE)
    @SerializedName("login")
    protected Login login;

    @SerializedName("registered")
    @DatabaseField(uniqueIndex = true, index = true, dataType = DataType.SERIALIZABLE)
    protected Registered registered;

    @SerializedName("dob")
    @DatabaseField(uniqueIndex = true, index = true, dataType = DataType.SERIALIZABLE)
    protected Dob dob;

    @SerializedName("phone")
    @DatabaseField
    protected String phone;

    @SerializedName("cell")
    @DatabaseField
    protected String cell;

    @SerializedName("SSN")
    @DatabaseField
    protected String sSN;

    @SerializedName("picture")
    @DatabaseField(dataType = DataType.SERIALIZABLE)
    protected Picture picture;

    @SerializedName("version")
    @DatabaseField
    protected String version;

    @DatabaseField
    protected boolean isFavorite;

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Name getName() {
        return name;
    }

    public void setName(Name name) {
        this.name = name;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Registered getRegistered() {
        return registered;
    }

    public void setRegistered(Registered registered) {
        this.registered = registered;
    }

    public Dob getDob() {
        return dob;
    }

    public void setDob(Dob dob) {
        this.dob = dob;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCell() {
        return cell;
    }

    public void setCell(String cell) {
        this.cell = cell;
    }

    public String getsSN() {
        return sSN;
    }

    public void setsSN(String sSN) {
        this.sSN = sSN;
    }

    public Picture getPicture() {
        return picture;
    }

    public void setPicture(Picture picture) {
        this.picture = picture;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Login getLogin() {
        return login;
    }

    public void setLogin(Login login) {
        this.login = login;
    }

    @Override
    public int compareTo(@NonNull User user) {
        return this.getName().compareTo(user.getName());
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }

    public int getId() {
        return _id;
    }

    public void setId(int id) {
        this._id = id;
    }
}