package com.woom.randomusers.domain.busresponses;

public class FavUserResponse extends DataResponse<Boolean> {

    public FavUserResponse(){
        super();
    }

    public FavUserResponse(Boolean user){
        super(user);
    }

    public FavUserResponse(DataResponse<Boolean> userDataResponse){
        super(userDataResponse);
    }
}
