package com.woom.randomusers.domain.busresponses;

import com.woom.randomusers.domain.model.User;
import com.woom.randomusers.domain.model.User;

import java.util.List;

public class UsersResponse extends DataResponse<List<User>> {
    public UsersResponse() {
        super();
    }

    public UsersResponse(List<User> users) {
        super(users);
    }

    public UsersResponse(DataResponse<List<User>> userDataResponse) {
        super(userDataResponse);
    }

    public UsersResponse(int error) {
        super(error);
    }
}
