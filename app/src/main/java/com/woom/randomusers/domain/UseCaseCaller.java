package com.woom.randomusers.domain;

import java.util.ArrayList;

/**
 * Interface to define a mehod to call an usecase interactor
 * Created by seven on 22/04/2017.
 */

public interface UseCaseCaller {

    void callUseCase(int useCaseNumber, boolean forceUpdate, ArrayList<String> params);
}
