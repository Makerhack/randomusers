package com.woom.randomusers.domain;

import android.os.Bundle;

import com.woom.randomusers.domain.UseCase;

import java.util.List;

public interface Executor {

    void execute(UseCase useCase, List<String> params);

}
