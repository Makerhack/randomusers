package com.woom.randomusers.domain.usecases;

import com.squareup.otto.Bus;
import com.woom.randomusers.domain.BaseUseCase;
import com.woom.randomusers.domain.BaseUseCase;
import com.woom.randomusers.domain.busresponses.UsersResponse;
import com.woom.randomusers.repository.UserRepository;

public class GetMoreUsersUseCase extends BaseUseCase<UsersResponse> {

    private UserRepository repository;

    public GetMoreUsersUseCase(Bus bus, UserRepository repository) {
        super(bus);
        this.repository = repository;
    }

    @Override
    public void execute() {
        handleResponse(repository.getMore());
    }
}
