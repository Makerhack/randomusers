package com.woom.randomusers.repository;

import com.woom.randomusers.domain.busresponses.DataResponse;
import com.woom.randomusers.domain.busresponses.UserDetailResponse;
import com.woom.randomusers.domain.busresponses.UsersResponse;
import com.woom.randomusers.domain.model.User;
import com.woom.randomusers.domain.busresponses.DataResponse;
import com.woom.randomusers.domain.busresponses.UserDetailResponse;
import com.woom.randomusers.domain.busresponses.UsersResponse;
import com.woom.randomusers.domain.model.User;

import java.util.List;

/**
 * Repository for quering user data
 * Created by seven on 22/04/2017.
 */

public class UserRepository implements Repository<User, String> {


    private ReadOnlyDataSource networkDataSource;
    private WritableDataSource databaseDataSource;
    private boolean forceNetwork = true;

    public UserRepository(ReadOnlyDataSource networkDataSource, WritableDataSource databaseDataSource) {
        this.networkDataSource = networkDataSource;
        this.databaseDataSource = databaseDataSource;
    }

    @Override
    public UserDetailResponse getByKey(String key) {
        return new UserDetailResponse(databaseDataSource.getUserById(key));
    }

    @Override
    public UsersResponse getOffline() {
        return new UsersResponse(databaseDataSource.getUsers());
    }

    @Override
    public UsersResponse getAll() {
        if (forceNetwork || databaseDataSource.isCacheInvalidated()) {
            //Update the users from the network and store them in the database
            DataResponse<List<User>> users = networkDataSource.getUsers();
            if (!users.hasError()) {
                //Store and then return the complete offline list
                databaseDataSource.storeUsers(users.getResponse());
            } else {
                return new UsersResponse(users);
            }
        }
        forceNetwork = false;
        return getOffline();
    }

    @Override
    public boolean deleteByKey(String key) {
        int deleted = databaseDataSource.deleteUser(key);
        return deleted > 0;

    }

    @Override
    public void deleteAll() {
        databaseDataSource.clealUsers();
    }

    @Override
    public UsersResponse getPaginated() {
        return null;
    }

    @Override
    public UsersResponse getMore() {
        forceNetwork = true;
        return getAll();
    }

    @Override
    public boolean save(User data) {
        return databaseDataSource.storeUser(data) > 0;
    }

    @Override
    public void saveBulk(List<User> data) {
        databaseDataSource.storeUsers(data);
    }

    public void clear(){
        databaseDataSource.clealUsers();
    }
}
