package com.woom.randomusers.repository.network.base;


import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.RequestFuture;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSyntaxException;
import com.woom.randomusers.view.Constants;
import com.woom.randomusers.view.Constants;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;


public class GsonRequest<T> extends JsonRequest<T> {
    public int id = 0;
    private Type type;


    /**
     * Make a POST request and return a parsed object from JSON.
     *
     * @param url  requestUrl of the request to make
     * @param type Relevant class object, for Gson's reflection
     */
    private GsonRequest(int method, String url, Type type, String requestBody, Response.Listener<T> listener,
                        Response.ErrorListener errorListener) {
        super(method, url, requestBody, listener, errorListener);
        this.type = type;
        Log.d(url, requestBody);
    }


    public int getId() {
        return id;
    }

    public void setId(int number) {
        this.id = number;
    }

    @SuppressWarnings("unchecked")
    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        try {
            String json = new String(response.data, "UTF-8");
            Log.d(getUrl(), json);
            T parsedResponse;
            try {
                parsedResponse = parseJson(json);
            } catch (JsonParseException ex) {
                ex.printStackTrace();
                return Response.error(new VolleyError(String.valueOf(Constants.Errors.ERROR_CANT_PARSE)));
            }
            if (parsedResponse != null) {
                return Response.success(parsedResponse, getCacheEntry());
            } else {
                Error error = new Gson().fromJson(json, Error.class);
                if (error != null) {
                    return Response.error(new VolleyError(error.getMessage()));
                }
                return Response.error(new VolleyError(String.valueOf(Constants.Errors.ERROR_CANT_PARSE)));
            }
        } catch (UnsupportedEncodingException | JsonSyntaxException e) {
            e.printStackTrace();
            return Response.error(new VolleyError(String.valueOf(Constants.Errors.ERROR_CANT_PARSE)));
        }
    }

    private T parseJson(String jsonString) throws JsonParseException {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(Integer.class, (JsonDeserializer<Integer>) (json, typeOfT, context) -> {
            try {
                return json.getAsInt();
            } catch (JsonSyntaxException | NumberFormatException ex) {
                ex.printStackTrace();
            }
            return 0;
        });
        Gson gson = builder.create();
        return gson.fromJson(jsonString, type);
    }

    @Override
    protected VolleyError parseNetworkError(VolleyError volleyError) {
        return new VolleyError(String.valueOf(Constants.Errors.ERROR_SERVICE_UNAVAILABLE));
    }

    public static class Builder<RType>{
        private Type type;
        private int method = 0;
        private String requestBody;
        private String requestUrl = "";

        public Builder(Type ReturnType, int method, String requestBody, String urlParams) {
            this.type = ReturnType;
            this.method = method;
            this.requestBody = requestBody;
            this.requestUrl = String.format(getUrl(), urlParams);

        }

        public GsonRequest<RType> buildFuture(RequestFuture<RType> futureRequest) {
            return new GsonRequest<>(method, requestUrl, type, requestBody, futureRequest,
                    futureRequest);
        }

        private String getUrl() {
            return NetworkConstants.BASE_URL;
        }

    }

}