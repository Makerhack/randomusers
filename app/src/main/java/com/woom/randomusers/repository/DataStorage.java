package com.woom.randomusers.repository;

import com.woom.randomusers.domain.model.User;
import com.woom.randomusers.domain.model.User;

import java.util.List;

public interface DataStorage {

    void storeUsers(List<User> transactions);
    int deleteUser(String userId);
    void clealUsers();
    int storeUser(User data);
}
