package com.woom.randomusers.repository;


import com.woom.randomusers.repository.ReadOnlyDataSource;
import com.woom.randomusers.repository.DataStorage;


/**
 * Created by seven on 22/04/2017.
 */

public interface WritableDataSource extends DataStorage, ReadOnlyDataSource {
    boolean isCacheInvalidated();
}
