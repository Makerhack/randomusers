package com.woom.randomusers.repository.local;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.misc.TransactionManager;
import com.j256.ormlite.stmt.QueryBuilder;
import com.woom.randomusers.domain.busresponses.DataResponse;
import com.woom.randomusers.domain.model.User;
import com.woom.randomusers.repository.WritableDataSource;
import com.woom.randomusers.view.Constants;
import com.woom.randomusers.domain.busresponses.DataResponse;
import com.woom.randomusers.domain.model.User;
import com.woom.randomusers.repository.WritableDataSource;
import com.woom.randomusers.view.Constants;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;

public class AndroidDatabaseSource implements WritableDataSource {

    private DataBaseHelper helper;
    private int order = 0;
    private boolean orderAsc;
    private boolean dataSetLoaded;

    public AndroidDatabaseSource(DataBaseHelper helper) {
        this.helper = helper;
    }


    public void storeUsers(final List<User> users) {
        final Dao<User, Integer> usersDao = helper.getUsersDao();
        try {
            TransactionManager.callInTransaction(helper.getConnectionSource(), new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    for (User transaction : users) {
                        usersDao.createOrUpdate(transaction);
                    }
                    return null;
                }
            });
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (users != null && !users.isEmpty()) {
            dataSetLoaded = true;
        }
    }

    public int storeUser(User user) {
        final Dao<User, Integer> usersDao = helper.getUsersDao();
        try {
            Dao.CreateOrUpdateStatus status = usersDao.createOrUpdate(user);
            return status.getNumLinesChanged();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return 0;
    }

    @Override
    public DataResponse<User> getUserById(String userId) {
        try {
            User user = helper.getUsersDao().queryForId(Integer.valueOf(userId));
            if (user != null) {
                return new DataResponse<>(user);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return new DataResponse<User>(Constants.Errors.NO_USER);
    }

    @Override
    public int deleteUser(String userId) {
        final Dao<User, Integer> usersDao = helper.getUsersDao();
        try {
            return usersDao.deleteById(Integer.valueOf(userId));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public void clealUsers() {
        try {
            helper.getUsersDao().deleteBuilder().delete();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public DataResponse<List<User>> getUsers() {
        final Dao<User, Integer> usersDao = helper.getUsersDao();
        QueryBuilder<User, Integer> queryBuilder = usersDao.queryBuilder().distinct();
        List<User> users = new ArrayList<>();
        try {
            users = queryBuilder.query();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (users != null && !users.isEmpty()) {
            users.sort(User::compareTo);
            if (orderAsc) {
                Collections.reverse(users);
            }
            return new DataResponse<>(users);
        }
        return new DataResponse<List<User>>(Constants.Errors.EMPTY_RESPONSE);
    }

    @Override
    public boolean hasUserDatasetLoaded() {
        return dataSetLoaded;
    }

    public String getOrderColumn() {
        switch (order) {
            default:
            case 0:
                return "name";
            case 1:
                return "gender";

        }
    }

    public void serOrder(int order, boolean ascending) {
        this.order = order;
        this.orderAsc = ascending;
    }

    public boolean isCacheInvalidated() {
        //TODO decide and implement a real caching system
        try {
            return helper.getUsersDao().countOf() <= 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

}
