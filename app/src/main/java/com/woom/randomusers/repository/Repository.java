package com.woom.randomusers.repository;

import com.woom.randomusers.domain.busresponses.DataResponse;
import com.woom.randomusers.domain.busresponses.DataResponse;
import com.woom.randomusers.domain.busresponses.UsersResponse;

import java.util.List;

/**
 * Inteface representing a data repository
 * Created by seven on 22/04/2017.
 */

public interface Repository<T, K> {

    DataResponse<T> getByKey(K key);

    DataResponse<List<T>> getAll();

    boolean deleteByKey(K key);

    void deleteAll();

    DataResponse<List<T>> getPaginated();

    DataResponse<List<T>> getMore();

    boolean save(T data);

    void saveBulk(List<T> data);

    DataResponse<List<T>> getOffline();
}
