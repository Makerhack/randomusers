package com.woom.randomusers.repository.network.base;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.woom.randomusers.domain.model.User;
import com.woom.randomusers.domain.model.User;

import java.util.List;

public class JsonModel {

    @SerializedName("results")
    @Expose
    private List<User> results = null;

    public List<User> getResults() {
        return results;
    }

}