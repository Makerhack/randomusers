package com.woom.randomusers.repository.network.base;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.RequestFuture;
import com.woom.randomusers.domain.busresponses.DataResponse;
import com.woom.randomusers.view.Constants;
import com.woom.randomusers.domain.busresponses.DataResponse;
import com.woom.randomusers.view.Constants;


import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Wrapper represenging a network connection
 * Created by Santi
 */
public class NetworkConnection {


    protected RequestQueue requestQueue;

    public NetworkConnection(RequestQueue requestQueue) {

        this.requestQueue = requestQueue;
    }

    protected <T> DataResponse<T> performNetworkRequest(GsonRequest.Builder<T> builder) {
        DataResponse<T> dataResponse = new DataResponse<>();
        RequestFuture<T> futureRequest = RequestFuture.newFuture();
        GsonRequest<T> request = builder.buildFuture(futureRequest);
        request.setRetryPolicy(new DefaultRetryPolicy(120000, 3, 0));
        futureRequest.setRequest(request);
        requestQueue.add(request);
        try {
            T t = futureRequest.get(120, TimeUnit.SECONDS);
            dataResponse.setResponse(t);
            dataResponse.setError(0);
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            e.printStackTrace();
            dataResponse.setError(Constants.Errors.ERROR_SERVICE_UNAVAILABLE);
        }
        return dataResponse;
    }

}
