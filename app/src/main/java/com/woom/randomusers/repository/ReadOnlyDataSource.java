package com.woom.randomusers.repository;

import com.woom.randomusers.domain.busresponses.DataResponse;
import com.woom.randomusers.domain.model.User;
import com.woom.randomusers.domain.model.User;
import com.woom.randomusers.domain.busresponses.DataResponse;

import java.util.List;

public interface ReadOnlyDataSource {

    DataResponse<List<User>> getUsers();

    DataResponse<User> getUserById(String id);

    boolean hasUserDatasetLoaded();

}
