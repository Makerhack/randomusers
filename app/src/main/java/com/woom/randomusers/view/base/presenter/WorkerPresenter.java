package com.woom.randomusers.view.base.presenter;

/**
 * Created by seven on 22/04/2017.
 */

public interface WorkerPresenter {
    int startWorking();

    void stopWorking(int index);

    boolean isWorking();

    void stopAllWorks();

    boolean hasLoadingView();
}
