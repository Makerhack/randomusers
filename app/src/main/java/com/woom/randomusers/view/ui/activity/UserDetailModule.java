package com.woom.randomusers.view.ui.activity;

import com.squareup.otto.Bus;
import com.woom.randomusers.domain.UseCaseCaller;
import com.woom.randomusers.view.base.annotations.MyPresenter;
import com.woom.randomusers.view.ui.fragment.UsersFragment;
import com.woom.randomusers.view.ui.fragment.UsertDetailFragment;
import com.woom.randomusers.view.ui.presenter.UsersPresenter;
import com.woom.randomusers.view.ui.presenter.UsertDetailPresenter;

import dagger.Module;
import dagger.Provides;

@Module(library = true, complete = false,
        injects = {UserDetailActivity.class, UsertDetailFragment.class})

public class UserDetailModule {

    @Provides
    public @MyPresenter
    UsertDetailPresenter provideProductsPresenter(Bus bus, UseCaseCaller caseCaller) {
        return new UsertDetailPresenter(bus, caseCaller);
    }
}
