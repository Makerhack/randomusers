package com.woom.randomusers.view.base.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.woom.randomusers.view.base.activity.DaggerActivity;
import com.woom.randomusers.view.base.activity.DaggerActivity;
import com.woom.randomusers.view.base.annotations.MyPresenter;
import com.woom.randomusers.view.base.presenter.Presenter;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

import butterknife.ButterKnife;


public abstract class PresenterFragment extends BaseFragment {

    private Presenter presenter;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        injectDependencies();
        presenter = getPresenter(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View v = inflater.inflate(getLayoutId(), container, false);
        ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (presenter != null) {
            presenter.onResume(this);
        }
    }

    @Override
    public void onPause() {
        if (presenter != null) {
            presenter.onPause();
        }
        super.onPause();
    }

    public abstract
    @LayoutRes
    int getLayoutId();

    private void injectDependencies() {
        ((DaggerActivity) getActivity()).inject(this);
    }


    public Presenter getPresenter(Object source) {
        for (Field field : source.getClass().getDeclaredFields()) {
            if (field.isAnnotationPresent(MyPresenter.class)) {
                if (Modifier.isPrivate(field.getModifiers())) {
                    throw new RuntimeException("Presenter field must be public");
                } else {
                    try {
                        field.setAccessible(true);
                        Presenter presenter = (Presenter) field.get(source);
                        field.setAccessible(false);
                        return presenter;
                    } catch (IllegalAccessException e) {
                        RuntimeException exception = new RuntimeException("Presenter field must be public");
                        exception.initCause(e);
                        throw exception;
                    }
                }
            }
        }
        return null;
    }

}
