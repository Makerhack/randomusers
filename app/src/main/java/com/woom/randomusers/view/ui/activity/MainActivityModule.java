package com.woom.randomusers.view.ui.activity;

import android.content.Context;

import com.squareup.otto.Bus;
import com.woom.randomusers.domain.UseCaseCaller;
import com.woom.randomusers.view.base.annotations.ActivityContext;
import com.woom.randomusers.view.base.annotations.MyPresenter;
import com.woom.randomusers.view.ui.fragment.UsersFragment;
import com.woom.randomusers.view.ui.presenter.UsersPresenter;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module(library = true, complete = false,
        injects = {MainActivity.class, UsersFragment.class})

public class MainActivityModule {

    @Provides
    public @MyPresenter UsersPresenter provideProductsPresenter(Bus bus, UseCaseCaller caseCaller) {
        return new UsersPresenter(bus, caseCaller);
    }
}
