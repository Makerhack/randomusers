package com.woom.randomusers.view.ui.presenter;

import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;
import com.woom.randomusers.domain.UseCaseCaller;
import com.woom.randomusers.domain.busresponses.UserDetailResponse;
import com.woom.randomusers.domain.model.DataError;
import com.woom.randomusers.domain.model.User;
import com.woom.randomusers.view.Constants;
import com.woom.randomusers.view.base.presenter.BasePresenter;

import java.util.Collections;
import java.util.List;

/**
 * Presemter for UsersFragment
 * Created by seven on 22/04/2017.
 */

public class UsertDetailPresenter extends BasePresenter<UsertDetailPresenter.View> {

    public UsertDetailPresenter(Bus bus, UseCaseCaller caller) {
        super(bus, caller);
    }

    public void getUser(String userId) {
        List<String> params = Collections.singletonList(userId);
        callUseCase(Constants.UseCases.GET_USER, params, false);
    }

    @Subscribe
    public void onSuccess(UserDetailResponse response) {
        super.onSuccess();
        if (isRegistered()) {
            view.onUser(response.getResponse());
        }
    }


    @Subscribe
    public void onError(DataError error) {
        super.onError(error);
        if (error.getErrorCode() == Constants.Errors.NO_USER) {
            if (isRegistered()) {
                view.showUserError();
            }
        }
    }


    public interface View extends BasePresenter.View {
        void onUser(User user);

        void showUserError();
    }


}
