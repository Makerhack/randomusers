package com.woom.randomusers.view.ui.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.woom.randomusers.view.Constants;
import com.woom.randomusers.R;
import com.woom.randomusers.domain.model.Location;
import com.woom.randomusers.domain.model.Picture;
import com.woom.randomusers.domain.model.Registered;
import com.woom.randomusers.domain.model.User;
import com.woom.randomusers.view.Constants;
import com.woom.randomusers.view.base.GlideApp;
import com.woom.randomusers.view.base.annotations.MyPresenter;
import com.woom.randomusers.view.base.fragment.PresenterFragment;
import com.woom.randomusers.view.ui.presenter.UsertDetailPresenter;

import org.w3c.dom.Text;

import java.io.Serializable;

import javax.inject.Inject;

import butterknife.BindView;

/**
 * Class showing a user extended information
 * Created by seven on 22/04/2017.
 */

public class UsertDetailFragment extends PresenterFragment implements UsertDetailPresenter.View {

    @Inject
    @MyPresenter
    protected UsertDetailPresenter presenter;

    @BindView(R.id.picture_iv)
    protected ImageView pictureIv;
    @BindView(R.id.gender_tv)
    protected TextView genderTv;
    @BindView(R.id.location_tv)
    protected TextView locationTv;
    @BindView(R.id.registered_tv)
    protected TextView registeredTv;
    @BindView(R.id.email_tv)
    protected TextView emailTv;
    @BindView(R.id.toolbar)
    protected Toolbar toolbar;
    @BindView(R.id.toolbar_layout)
    protected CollapsingToolbarLayout toolbarLayout;

    public static final String TAG = UsertDetailFragment.class.getSimpleName();
    private User user;
    private String userId;

    public static UsertDetailFragment newInstance(String userId) {
        Bundle args = new Bundle();
        args.putString(Constants.Keys.USER_ID, userId);
        UsertDetailFragment fragment = new UsertDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arguments = getArguments();
        if (arguments != null) {
            userId = arguments.getString(Constants.Keys.USER_ID);
        }
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey(Constants.Keys.USER_ID)) {
                userId = savedInstanceState.getString(Constants.Keys.USER_ID);
            }
            if (savedInstanceState.containsKey(Constants.Keys.USER)) {
                user = (User) savedInstanceState.getSerializable(Constants.Keys.USER);
            }
        }
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_user_detail;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        toolbarLayout.setCollapsedTitleTextColor(Color.WHITE);
        toolbarLayout.setExpandedTitleColor(Color.WHITE);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (user == null) {
            presenter.getUser(userId);
        } else {
            drawUser();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(Constants.Keys.USER_ID, userId);
        outState.putSerializable(Constants.Keys.USER, (Serializable) user);
        super.onSaveInstanceState(outState);
    }

    private void drawUser() {
        if (user == null || !isAdded()) {
            return;
        }
        toolbarLayout.setTitle(user.getName().getFirst() + " " + user.getName().getLast());

        Picture picture = user.getPicture();
        if (picture != null && !TextUtils.isEmpty(picture.getThumbnail())) {
            GlideApp.with(getContext()).load(picture.getLarge()).into(pictureIv);
        }
        String gender = user.getGender();
        if (!TextUtils.isEmpty(gender)) {
            genderTv.setText(gender);
        }
        Location location = user.getLocation();
        if (location != null) {
            String locationString = location.getStreet() + " " + location.getCity() + " " + location.getState();
            if (!TextUtils.isEmpty(locationString)) {
                Uri gmmIntentUri = Uri.parse("geo:0,0?q=1600"+ locationString);
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                locationTv.setText(locationString);
                locationTv.setOnClickListener((v)->startActivity(mapIntent));
            }
        }
        Registered registered = user.getRegistered();
        if (registered != null) {
            registeredTv.setText(registered.getDate());
        }
        String email = user.getEmail();
        if (!TextUtils.isEmpty(email)) {
            Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
            emailIntent.setData(Uri.parse("mailto:"+email));
            emailTv.setText(email);
            emailTv.setOnClickListener((v)->startActivity(emailIntent));
        }
    }

    @Override
    protected void refresh() {
        presenter.getUser(userId);
    }

    @Override
    public void onUser(User user) {
        this.user = user;
        if (isAdded()) {
            drawUser();
        }
    }

    @Override
    public void showUserError() {
        showMessage(R.string.no_user);
        if (isAdded()) {
            getActivity().finish();
        }
    }
}
