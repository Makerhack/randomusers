package com.woom.randomusers.domain;

import com.woom.randomusers.domain.busresponses.DataResponse;
import com.woom.randomusers.domain.model.Dob;
import com.woom.randomusers.domain.model.Name;
import com.woom.randomusers.domain.model.Registered;
import com.woom.randomusers.domain.model.User;
import com.woom.randomusers.repository.ReadOnlyDataSource;
import com.woom.randomusers.repository.WritableDataSource;
import com.woom.randomusers.view.Constants;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * Created by seven on 22/04/2017.
 */

public class MockLocalDataSource implements ReadOnlyDataSource{
    
    private List<User> users;
    
    public MockLocalDataSource(){
        users = new ArrayList<>();
        User user1 = new User();
        user1.setId(1);
        Name name = new Name();
        name.setFirst("Fake 1");
        name.setLast("FakeLast");
        Dob dob = new Dob();
        dob.setDate("1988-01-05");
        user1.setDob(dob);
        Registered registered = new Registered();
        registered.setDate("2021-12-12");
        user1.setRegistered(registered);
        user1.setName(name);
        user1.setEmail("fake@fake.com");
        User user2 = new User();
        Name name2 = new Name();
        name2.setFirst("Fake 2");
        name2.setLast("FakeLast2");
        Dob dob2 = new Dob();
        dob2.setDate("1989-01-05");
        user2.setDob(dob2);
        Registered registered2 = new Registered();
        registered2.setDate("2022-12-12");
        user2.setRegistered(registered2);
        user2.setName(name2);
        user2.setEmail("fake@fake.com");
        users.add(user2);
        User user3 = new User();
        user3.setId(1);
        Name name3 = new Name();
        name.setFirst("Fake 1");
        name.setLast("FakeLast");
        Dob dob3 = new Dob();
        dob3.setDate("1990-01-05");
        user3.setDob(dob3);
        Registered registered3 = new Registered();
        registered3.setDate("2024-12-12");
        user3.setRegistered(registered3);
        user3.setName(name3);
        user3.setEmail("fake@fake.com");
        users.add(user3);
    }
    

    @Override
    public DataResponse<List<User>> getUsers() {
        return new DataResponse<>(users);
    }

    @Override
    public DataResponse<User> getUserById(String id) {
        User user = users.stream().filter(it -> it.getId() == Integer.valueOf(id)).findFirst().orElse(null);
        if (user == null) {
            return new DataResponse<User>(Constants.Errors.NO_USER);
        }
        return new DataResponse<>(user);
    }

    @Override
    public boolean hasUserDatasetLoaded() {
        return true;
    }
    
}
